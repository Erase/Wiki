<table>
	<tr>
		<th><?= t('Nom');?></th>
		<th><?= t('Titre');?></th>
		<th><?= t('Dernière modification');?></th>
		<th><?= t('Nombre de modifications');?></th>
	</tr>
	<?php foreach($data as $article): if ((checklogin()|| check($CONFIG['openread'])) and (checklogin() || check($article['openread']))): ?>
			<tr>
				<td data-title="<?= t('Nom');?>"><a href="<?= url($article['slug']); ?>" target="_blank"><?= $article['slug']; ?></a></td>
				<td data-title="<?= t('Titre');?>"><?=$article['title']; ?></td>
				<td data-title="<?= t('Dernière modification');?>"><?=format_date($article['time']); ?></td>
				<td data-title="<?= t('Nombre de modifications');?>"><?=$article['nb'];?></td>
			</tr>
		<?php endif; endforeach;?>
</table>
