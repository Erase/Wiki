<?php
set_time_limit(0);
session_start();
include 'app/inc/inc.php';

if (isset($_GET['do'])) {
    switch ($_GET['do']) {
        case 'upload':
            ##### UPLOAD ######
            // curl -i -X POST -H "Content-Type: multipart/form-data"  -F "image=@4ba9b82ec3b76fff.png" http://qwerty.legtux.org/varia/wiki/api.php?do=upload
            if (check(@$_SESSION['login'])) {
                if (isset($_FILES['image'])) {
                    $uploaddir = 'data/files/';
                    $filename = int_to_alph(time()).'_'.basename($_FILES['image']['name']);
                    @rename($_FILES['image']['name'], 'data/files/'.$filename);
                    //$filename = basename($_FILES['image']['name']);
                    $uploadfile = $uploaddir.$filename;
                   /* if (in_array($_FILES['image']['type'], $blacklist_mimetype)) {
                        echo json_encode(array('error'=>'typeNotAllowed'));
                    } */
                    if (!is_uploaded_file($_FILES['image']['tmp_name'])) {
                        echo json_encode(array('error'=>'importError'));
                        echo "can't upload";
                    }
                    if (move_uploaded_file($_FILES['image']['tmp_name'], $uploadfile)) {
                        echo json_encode(array('data'=> array('filePath'=>'data/files/'.$filename)));
                    }
                }
            } else {
                exit('error');
            }
        break;
        case 'feed':
        	header('Content-Type: application/atom+xml;charset=utf-8');
        	$query = $db->prepare('SELECT * FROM article ORDER BY time DESC');
			$query->execute();
			$data = $query->fetchAll();
			include 'app/templates/atom.php';
        break;
        case 'export':
            if (checklogin()) {
                if (isset($_GET['id'])) {
                    $filename = $_GET['id'].'.md';
                    $query = $db->prepare('SELECT * FROM article WHERE slug=? ORDER BY time DESC LIMIT 1');
					$query->execute([$_GET['id']]);
					$data = $query->fetch(PDO::FETCH_ASSOC);
                    header("Content-disposition: attachment; filename=$filename");
                    header('Content-type:text/plain;charset=utf-8');
                    $export = '---'.PHP_EOL;
                    $export .= 'title: "'.$data['title'].'"'.PHP_EOL;
                    $export .= 'date: '.date("Y-m-d", $data['time']).PHP_EOL;
                    $export .= '---'.PHP_EOL;
                    $export .= $data['content'];
                    echo $export;
                }
            }
        break;
        case 'backup':
        	 if (checklogin()) {
                if (!is_dir('backups')) {
                    mkdir('backups');
                }
                $filename = 'backups/backup-'.date('YmdHis', time()).'.zip';
                ZipDir($filename, 'data/');
                header("Location: ".$filename);
            }
        break;
        case 'file2sql':
        	echo '<p>Il est nécessaire d\'avoir crée au préalable/installé la version SQL et d\'importer le dossier data/ de la version plain texte sous le nom data_files</p>
        	<form method="post" action="api.php?do=file2sql">
        	<input type="submit" value="Transférer la configuration" name="config" id="config"/>
        	<input type="submit" value="Transférer les fichiers" name="file" id="file"/>
        	<input type="submit" value="Transférer les pages" name="page" id="page"/>
        	</form>';
       		if(isset($_POST['page'])) {
				foreach(array_diff(scandir('data_files/pages/', SCANDIR_SORT_DESCENDING), array('..', '.')) as $pages) {
					foreach(array_diff(scandir('data_files/pages/'.$pages, SCANDIR_SORT_DESCENDING), array('..', '.')) as $page) {
						$article = json_decode(file_get_contents('data_files/pages/'.$pages.'/'.$page), true);
						$openread = ($article['openread']==true) ? 1: 0;
						sql('INSERT INTO article(slug, title, content, time, ip, comment, openread) VALUES (?, ?, ?, ?, ?, ?, ?);',[$pages, $article['title'], $article['content'], $article['time'], $article['ip'], $article['comment'], $openread]);
					}
				}
			}
       		if(isset($_POST['config'])) {
       			$old_config = file_get_contents('data_files/config.php');
        		$old_config = str_replace('<?php /* ', '', $old_config);
        		$old_config = str_replace(' */ ?>', '', $old_config);
        		$old_config = json_decode($old_config, true);
        	
        		$old_config['urlrewrite'] = ($old_config['urlrewrite'] == true) ? '1' : '0';
        		$old_config['openread'] = ($old_config['openread'] == true) ? '1' : '0';
        
        		sql('UPDATE config SET value = ? WHERE key = "title"', [$old_config['title']]);
        		sql('UPDATE config SET value = ? WHERE key = "email"', [$old_config['email']]);
        		sql('UPDATE config SET value = ? WHERE key = "root"', [$old_config['root']]);
        		sql('UPDATE config SET value = ? WHERE key = "urlrewrite"', [$old_config['urlrewrite']]);
        		sql('UPDATE config SET value = ? WHERE key = "openread"', [$old_config['openread']]);
        		sql('UPDATE config SET value = ? WHERE key = "lang"', [$old_config['lang']]);
        		sql('UPDATE config SET value = ? WHERE key = "timezone"', [$old_config['timezone']]);
        		sql('UPDATE config SET value = ? WHERE key = "pwd"', [$old_config['pwd']]);
        		sql('UPDATE config SET value = ? WHERE key = "otp_enable"', [$old_config['otp']['otp_enable']]);
        		sql('UPDATE config SET value = ? WHERE key = "otp_gap"', [$old_config['otp']['otp_gap']]);
        		sql('UPDATE config SET value = ? WHERE key = "otp_token"', [$old_config['otp']['otp_token']]);
        		copy('data_files/style.css', 'data/style.css');
       		}
			if(isset($_POST['file'])) {
				$files = array_diff(scandir('data_files/files/', SCANDIR_SORT_DESCENDING), array('..', '.'));
				foreach($files as $file) {
					copy('data_files/files/'.$file, 'data/files/'.$file);
				}
			}
        break;
        case 'dev':
			print_r(setlocale(LC_ALL, 0));
		break;
		case 'translate':
			echo '<form method="post" action="api.php?do=translate">
        	<input type="submit" value="Générer les fichiers langues en JSON" name="json" id="json"/>
        	</form>';
			if(isset($_POST['json'])) {
				foreach(array_diff(scandir('app/locales/', SCANDIR_SORT_DESCENDING), array('..', '.', 'main.pot')) as $lang) {
					echo $lang.'<br>';
					rm('app/locales/'.$lang.'/LC_MESSAGES/main.json');
					$translations = (new Gettext\Loader\PoLoader())->loadFile('app/locales/'.$lang.'/LC_MESSAGES/main.po');
					(new Gettext\Generator\JsonGenerator())->generateFile($translations, 'app/locales/'.$lang.'/LC_MESSAGES/main.json');
				}
			}
			echo '<form method="post" action="api.php?do=translate">
			<label for="code">Code ISO<input type="text" name="code" id="code"/></label>
        	<input type="submit" value="Créer une nouvelle langue" name="create" id="create"/>
        	</form>';
        	if(isset($_POST['code'])) {
        		mkdir('app/locales/'.$_POST['code'], 0755);
        		mkdir('app/locales/'.$_POST['code'].'/LC_MESSAGES', 0755);
        		copy('app/locales/main.pot', 'app/locales/'.$_POST['code'].'/LC_MESSAGES/main.po');
        		echo 'Le fichier <code>app/locales/'.$_POST['code'].'/LC_MESSAGES/main.po</code> a été crée';
        	}
		break;
    }
} else {
    redirect($CONFIG['root']);
}
