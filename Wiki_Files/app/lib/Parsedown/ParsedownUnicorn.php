<?php
class ParsedownUnicorn extends \ParsedownToC {
	public function __construct(array $configurations = null) {
		parent::__construct();

		$this->InlineTypes['='][] = 'MarkText';
		$this->inlineMarkerList .= '=';
		$this->InlineTypes['^'][] = 'SuperText';
		$this->inlineMarkerList .= '^';
		$this->InlineTypes['~'][] = 'SubText';
	}

	public function text($text) {
		$markup = parent::text($text);
		$markup = $this->checkboxes($markup);
		return $markup;
	}
	protected function checkboxes($markup) {
			return str_replace(array('[ ]', '[x]', '[X]'), array('<input type="checkbox">', '<input type="checkbox" checked>', '<input type="checkbox" checked>'), $markup);
	}

 // Source : https://github.com/BenjaminHoegh/ParsedownExtended/blob/master/ParsedownExtended.php

	protected function inlineSuperText($Excerpt) {
		if (preg_match('/(?:\^(?!\^)([^\^ ]*)\^(?!\^))/', $Excerpt['text'], $matches)) {
			return array(
				'extent' => strlen($matches[0]),
				'element' => array(
					'name' => 'sup',
					'text' => $matches[1],
					'function' => 'lineElements'
				),

			);
		}
	}
	protected function inlineSubText($Excerpt) {
		if (preg_match('/(?:~(?!~)([^~ ]*)~(?!~))/', $Excerpt['text'], $matches)) {
			return array(
				'extent' => strlen($matches[0]),
				'element' => array(
					'name' => 'sub',
					'text' => $matches[1],
					'function' => 'lineElements'
				),

			);
		}
	}
	protected function inlineMarkText($Excerpt) {
		if (preg_match('/^(==)([^=]*?)(==)/', $Excerpt['text'], $matches)) {
			return array(
				'extent' => strlen($matches[0]),
				'element' => array(
					'name' => 'mark',
					'text' => $matches[2]
				),
			);
		}
	}
}
