<form method="get" action="<?=$CONFIG['root'];?>?do=history">
	<input type="submit" value="Comparer les versions"/>
	<input type="hidden" name="do" value="history"/>
	<table>
		<tr>
			<th><?= t('Comparer');?></th>
			<th><?= t('Nom');?></th>
			<th><?= t('Dernière modification');?></th>
			<th><?= t('Commentaire');?></th>
			<th><?= t('IP');?></th>
			<th></th>
		</tr>
		<?php foreach($data as $article): if ((checklogin()|| check($CONFIG['openread'])) and (checklogin() || check($article['openread']))): ?>
		<tr>
			<td data-title="<?= t('Comparer');?>"><input type="radio" name="old" value="<?=$article['id'];?>"><input type="radio" name="new" value="<?=$article['id'];?>"></td>
			<td data-title="<?= t('Nom');?>"><a href="<?=url($article['slug']);?>" target="_blank" <?= strlen($article['content']) == 0 ? 'class="empty"':'';?>><?=$article['slug'];?></a></td>
			<td data-title="<?= t('Dernière modification');?>"><a href="?do=history&view=<?=$article['id'];?>"><?=format_date($article['time']);?></a></td>
			<td data-title="<?= t('Commentaire');?>"><?=$article['comment'];?></td>
			<td data-title="<?= t('IP');?>"><?=$article['ip'];?></td>
			<td>
            <?php if(checklogin()): ?>
            <a href="?do=history&delete=<?=$article['id'];?>&token=<?=$_SESSION['token'];?>"><?=t('Supprimer');?></a> <a href="?do=history&restore=<?=$article['id'];?>&token=<?=$_SESSION['token'];?>"><?=t('Restaurer cette version');?></a>
            <?php endif; ?>
            </td>
		</tr>
		<?php endif; endforeach;?>
	</table>
</form>
