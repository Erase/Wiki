<?php

$exclude_scan = array(
    '.',
    '..',
    'config.php',
    'files',
    'log.log',
    'index.html',
    'favicon.ico',
    'style.css',
    'tracker.txt'
);
$blacklist_mimetype = array(
    'text/html',
    'text/javascript',
    'text/x-javascript',
    'application/x-shellscript',
    'application/x-php',
    'text/x-php',
    'text/x-python',
    'text/x-perl',
    'text/x-bash',
    'text/x-sh',
    'text/x-csh',
    'text/scriptlet',
    'application/x-msdownload',
    'application/x-msmetafile'
);


/*// ISO 639-1 https://fr.wikipedia.org/wiki/Liste_des_codes_ISO_639-1
$lang_list = [
    'en'	=>	'English',
    'fr'	=>	'Français',
    'oc'	=>	'Occitan',
    'la'	=>	'Latin',
    //'eo'	=>	'Esperanto',
];
*/
################ Fonctions générales ################
function media($media) {
    $picture_mimetype = array(
        'image/apng',
        'image/bmp',
        'image/gif',
        'image/x-icon',
        'image/jpeg',
        'image/png',
        'image/svg+xml',
        'image/tiff',
        'image/webp',
    );
    $audio_mimetype = array(
        'audio/aac',
        'audio/midi',
        'audio/ogg',
        'audio/mp3',
        'audio/mpeg',
        'audio/x-wav',
        'audio/webm',
        'audio/3gpp',
        'audio/3gpp3'
    );
    $video_mimetype = array(
        'video/x-msvideo',
        'video/mpeg',
        'video/ogg',
        'video/webm',
        'video/mp4',
        'video/3gpp',
        'video/3gpp2',
        'video/x-matroska'
    );
    if (in_array(mime_content_type($media), $picture_mimetype)) {
        return 'picture';
    } elseif (in_array(mime_content_type($media), $video_mimetype)) {
        return 'video';
    } elseif (in_array(mime_content_type($media), $audio_mimetype)) {
        return 'audio';
    } else {
        return '';
    }
}

function affmedia($media) {
    if (media($media) == 'picture') {
        return '<img src="'.$media.'" style="max-height:100px;" alt="picture"/>';
    } elseif (media($media) == 'video') {
        return '<video controls src="'.$media.'" style="max-width:100%;"></video>';
    } elseif (media($media) == 'audio') {
        return '<audio controls src="'.$media.'" style="max-width:100%;"></audio>';
    } else {
        return '';
    }
}
function mdmedia($media) {
    if (media($media) == 'picture') {
        return '!['.$media.']('.$media.')';
    } else {
        return $media;
    }
}
function config_check($function)
{
    if (function_exists($function)) {
        echo '<p>Function <b>'.$function.'</b> is <span style="color:green">OK</span></p>';
    } else {
        echo '<p>Function <b>'.$function.'</b> is <span style="color:red">NOT OK</span></p>';
    }
}
function check($var)
{
    if ((isset($var) || !empty($var)) and $var == true) {
        return true;
    } else {
        return false;
    }
}
function mylog($post)
{
    global $config;
    error_log('['.date('c', time()).'] ('.$_SERVER['REMOTE_ADDR'].') '.$post.PHP_EOL, 3, 'data/log.log');
    if ($config['notification'] == true and !in_array($_SERVER['REMOTE_ADDR'], explode(' ', $config['ip_whitelist'])) and $config['email'] != 'no-reply@example.org') {
        @error_log('['.date('c', time()).'] ('.$_SERVER['REMOTE_ADDR'].') '.$post.PHP_EOL, 1, $config['email']);
    }
}

function a($url, $str, $add='')
{
    return '<a href="'.$url.'" '.$add.'>'.$str.'</a>';
}

function beautiful_timezone_list()
{
    $tz = timezone_identifiers_list();
    $array = array();
    foreach ($tz as $v) {
        $codename = $v;
        $v = str_replace('_', ' ', $v);
        $v = str_replace('/', ' - ', $v);
        $array[$codename] = $v;
    }
    return $array;
}

################ IP ################

function geoip($ip)
{
    global $config;
    global $_SESSION;
    if (check(@$_SESSION['login']) || $config['show_public_ip'] == true) {
        return a('http://viewdns.info/iplocation/?ip='.$ip, $ip);
    } else {
        return '';
    }
}

################ AUTHENTIFICATION ################
function createOTP()
{
    global $config;
    $totp = OTPHP\TOTP::create($config['otp']['otp_token'], 30, 'sha1', 6);
    $totp->setLabel($config['email']);
    $totp->setIssuer($config['title']);
    return $totp->getProvisioningUri();
}
function checkOTP($otp)
{
    global $config;
    $decalage = $config['otp']['otp_gap'];  // (en secondes)
    $maintenant = time() + $decalage;
    $totp = OTPHP\TOTP::create($config['otp']['otp_token']);
    return $totp->verify($otp, $maintenant);
}

function auth_otp($otp)
{
    global $config;
    if ($config['otp']['otp_enable'] == true) {
        return checkOTP($otp);
    } else {
        return true;
    }
}
function check_auth($pwd, $otp)
{
    global $config;
    if (password_verify($pwd, $config['pwd'])) {
        if (@auth_otp($otp)) {
            $_SESSION['login'] = true;
            $_SESSION['token'] = md5(uniqid(rand(), true));
            mylog(t('auth.successfullogin'));
            return true;
        } else {
            mylog(t('auth.error.badOTP'));
            echo t('auth.error.badOTP');
            return false;
        }
    } else {
        mylog(t('auth.error.fail_connexion'));
        echo t('auth.error.fail_connexion');
        return false;
    }
}
function checklogin()
{
    if (check(@$_SESSION['login'])) {
        return true;
    } else {
        return false;
    }
}
function authcookie()
{
    global $config;
    if (isset($_COOKIE['auth']) && !isset($_SESSION['login'])) {
        if ($_COOKIE['auth'] == sha1($config['salt'].$config['pwd'])) {
            $_SESSION['login'] = true;
            $_SESSION['token'] = md5(uniqid(rand(), true));
            mylog('[cookie] '.t('auth.successfullogin'));
        } else {
            unset($_COOKIE['auth']);
            setcookie('auth', '', time() - 3600, null, null, false, true);
        }
    }
}
function logout($redirect)
{
    mylog(t('auth.logout'));
    unset($_SESSION['login']);
    unset($_SESSION['token']);
    unset($_COOKIE['auth']);
    setcookie('auth', '', time() - 3600, null, null, false, true);
    session_destroy();
    redirect($redirect);
    exit();
}
function genqrcode($str)
{
    return '<img src="#" id="qrcode"/><script>var url = QRCode.generatePNG("'.$str.'", {ecclevel: "M",margin: 4,modulesize: 4});document.getElementById("qrcode").src = url;</script>';
}
################    TRADUCTION   ################
function lang_list()
{
    $list = array();
    foreach (scandir('app/i18n') as $langfile) {
        if (!in_array($langfile, array('.','..'))) {
            $langname = json_decode(file_get_contents('app/i18n/'.$langfile), true);
            $list[basename($langfile, '.json')] = $langname['lang.name'];
        }
    }
    return $list;
}
function i18n()
{
    global $lang_list;
    foreach ($lang_list as $code => $name) {
        if (file_exists('app/i18n/'.$code.'.json')) {
            $lang[$code] = json_decode(file_get_contents('app/i18n/'.$code.'.json'), true)['translation'];
        }
    }
    return $lang;
}
function t($string)
{
    global $lang;
    global $config;
    global $lang_list;
    if ($config['lang'] == 'auto') {
        $browser_lang = @substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
        $i18n = (array_key_exists($browser_lang, $lang_list)) ? $browser_lang : 'en';
    } else {
        $i18n = $config['lang'];
    }
    return (!isset($lang[$i18n][$string]) || $lang[$i18n][$string] == '') ? '#~#'.$string.'#~#' : $lang[$i18n][$string];
}

################ GESTION DU WIKI ################
function install($pwd="demo")
{
    if (!file_exists('data')) {
        mkdir('data', 0755);
        mkdir('data/pages', 0755);
        mkdir('backups', 0755);
        file_put_contents('data/index.html', '');
        file_put_contents('data/style.css', '');
        file_put_contents('data/tracker.txt', '');
        mkdir('data/files', 0755);
        file_put_contents('data/files/index.html', '');
        $config = array(
            'title'			=> 'My Wiki',
            'root'			=> str_replace('/index.php', '', getRequestProtocol().'://'.$_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF']),
            'urlrewrite'	=> true,
            'email'			=> 'no-reply@example.org',
            'notification'	=> false,
            'ip_whitelist'	=> '',
            'stylesheet'	=> 'style.css',
            'history'		=> true,
            'openread'		=> true,
            'openwrite'		=> false,
            'easymde'		=> true,
            'scientific'	=> true,
            'show_public_ip'=> false,
            'lang' 			=> 'auto',
            'timezone'		=> date_default_timezone_get(),
            'pwd'			=> password_hash($pwd, PASSWORD_DEFAULT),
            'salt'			=> sha1(uniqid(rand(), true)),
            'otp'			=> array(
                            'otp_enable'	=> false,
                            'otp_gap' 		=>0,
                            'otp_token'		=> generateRandomString(32))

        );
        write_config($config);
        $article = array(
                    'title'=>'Welcome ! It’s a new page',
                    'content'=>'It’s a new page. Why not edit this page?',
                    'time'=>time(),
                    'openread'=> true,
                    'ip'=>$_SERVER['REMOTE_ADDR'],
                    'comment'=>'Creation'
                );
        write_article('index', $article);
        mylog(sprintf(t('content.create_page'), 'index'));
    }
    if (!file_exists('.htaccess')) {
        $htaccess = '<IfModule mod_rewrite.c> 
	RewriteEngine On
	RewriteCond %{REQUEST_FILENAME} !-f
	RewriteCond %{REQUEST_FILENAME} !-d
	RewriteRule ^(.*)$  index.php?id=$1 [L]
	# RewriteCond %{HTTPS} off
	# RewriteRule ^(.*)$ https://%{HTTP_HOST}%{REQUEST_URI} [L,R=301]
</IfModule>
<IfModule mod_headers.c>
	Header set Set-Cookie HttpOnly;Secure
    # Header set Content-Security-Policy "base-uri \'self\';"
    #  Header set Content-Security-Policy "script-src \'self\';"
    # Header set Content-Security-Policy "style-src \'self\';"
    Header set X-Content-Type-Options nosniff
    Header set X-Frame-Options DENY
    Header set Strict-Transport-Security "max-age=15811200" env=HTTPS
    Header always set X-XSS-Protection "1; mode=block"
</IfModule>';
        file_put_contents('.htaccess', $htaccess);
    }
}

function read_config()
{
    if (file_exists('data/config.php')) {
        $data = file_get_contents('data/config.php');
        $data = str_replace('<?php /* ', '', $data);
        $data = str_replace(' */ ?>', '', $data);
        $data = json_decode($data, true);
    } else {
        $data = array(
            'title'			=> 'My Wiki',
            'root'			=> str_replace('/index.php', '', getRequestProtocol().'://'.$_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF']),
            'email'			=> 'no-reply@example.org',
            'stylesheet'	=> 'style.css',
            'easymde'		=> true,
            'notification'	=> true,
            'scientific'	=> true,
            'openwrite'		=> false,
            'openread'		=> true,
            'lang' 			=> 'auto',
            'timezone'		=> date_default_timezone_get(),
        );
    }
    return $data;
}

function write_config($data)
{
    file_put_contents('data/config.php', '<?php /* '.json_encode($data).' */ ?>');
}

function read_article($file)
{
    if (file_exists('data/pages/'.$file)) {
        return  json_decode(file_get_contents('data/pages/'.$file.''), true);
    } else {
        return true;
    }
}
function write_article($file, $array=array())
{
    if (!is_dir('data/pages/'.$file)) {
        mkdir('data/pages/'.$file, 0755);
    }
    file_put_contents('data/pages/'.$file.'/content.md', json_encode($array));
}
function export($id)
{
    global $config;
    $filename = $id.'.md';
    //header("Content-disposition: attachment; filename=$filename");
    //header('Content-type:text/plain;charset=utf-8');
    $content = read_article($id.'/content.md');
    $export = '---'.PHP_EOL;
    $export .= (isset($content['title'])) ? 'title: '.$content['title'].PHP_EOL : '';
    $export .= 'author: '.PHP_EOL;
    $export .= "\t".' - '.$config['root'].'/'.$id.PHP_EOL;
    $export .= 'date: '.date('c', $content['time']).PHP_EOL;
    $export .= '...'.PHP_EOL.PHP_EOL;
    $export .= $content['content'];
    return $export;
}
function parse($str)
{
    global $config;
    $md = new \ParsedownUnicorn();
    $body = trim($md->text($str));
    $toc  = ($md->contentsList() !='') ? "\t\t".'<aside id="toc">'.PHP_EOL."\t\t\t".'<h1>'.t('content.toc').'</h1>'.PHP_EOL."\t\t\t".$md->contentsList('string').PHP_EOL."\t\t".'</aside>'.PHP_EOL : '';
    $str = PHP_EOL.$toc.PHP_EOL.$body;
    return $str;
}

function diffcompare($old, $new)
{
    $granularity = new bariew\FineDiff\Granularity\Character();
    $diff        = new bariew\FineDiff\Diff($granularity);
    return nl2br($diff->render($old, $new));
}

function timeAgo($time)
{
    $timestamp = time()-$time;
    $s = array(
                    'year' 		=> floor($timestamp/31536000),
                    'month'		=> floor($timestamp/2628000),
                    'week'		=> floor($timestamp/604800),
                    'day'		=> floor($timestamp/86400),
                    'hour'		=> floor($timestamp/3600),
                    'minute'	=> floor($timestamp/60),
                    'second'	=> floor($timestamp/1));

    if ($s['year'] >= 1) {
        return ($s['year'] > 1) ? sprintf(t('content.timeago.years'), $s['year']) : sprintf(t('content.timeago.year'), $s['year']);
    } elseif ($s['month'] >= 1) {
        return ($s['month'] > 1) ? sprintf(t('content.timeago.months'), $s['month']) : sprintf(t('content.timeago.month'), $s['month']);
    } elseif ($s['week'] >= 1) {
        return ($s['week'] > 1) ? sprintf(t('content.timeago.weeks'), $s['week']) : sprintf(t('content.timeago.week'), $s['week']);
    } elseif ($s['day'] >= 1) {
        return ($s['day'] > 1) ? sprintf(t('content.timeago.days'), $s['day']) : sprintf(t('content.timeago.day'), $s['day']);
    } elseif ($s['hour'] >= 1) {
        return ($s['hour'] > 1) ? sprintf(t('content.timeago.hours'), $s['hour']) : sprintf(t('content.timeago.hour'), $s['hour']);
    } elseif ($s['minute'] >= 1) {
        return ($s['minute'] > 1) ? sprintf(t('content.timeago.minutes'), $s['minute']) : sprintf(t('content.timeago.minute'), $s['minute']);
    } elseif ($s['second'] >= 1) {
        return ($s['second'] > 1) ? sprintf(t('content.timeago.seconds'), $s['second']) : sprintf(t('content.timeago.second'), $s['second']);
    } else {
        return sprintf(t('content.timeago.second'), $s['second']);
    }
}

function format_date($time)
{
    return '<time datetime="'.date('c', $time).'" title="'.date('c', $time).'">'.timeAgo($time).'</time>';
}

function stylesheetlist()
{
    $stylesheet = glob('app/assets/*.{css}', GLOB_BRACE);
    $return = array();
    foreach ($stylesheet as $ss) {
        $return[] = basename($ss);
    }
    return $return;
}
function purge_history()
{
    foreach (scandir('data') as $page) {
        if (!in_array($page, array('.','..', 'config.php', 'files', 'log.log', 'index.html', 'favicon.ico', 'style.css'))) {
            foreach (scandir('data/pages/'.$page) as $file) {
                if (!in_array($file, array('.','..', 'content.md'))) {
                    unlink('data/pages/'.$page.'/'.$file).PHP_EOL;
                }
            }
        }
    }
}

function check_newversion() {
    global $config;
    $new_version = file_get_contents('https://framagit.org/qwertygc/Wiki/raw/master/version');
    $current_version = file_get_contents('version');
    if ($new_version > $current_version) {
        return '<div class="block">'.a('https://framagit.org/qwertygc/Wiki', sprintf(t('config.new_version'), $current_version, $new_version)).' '.a($config['root'].'/api.php?do=update', t('config.update')).'</div>';
    }
}

function updater()
{
    global $config;
    $old = file_get_contents('version');
    $new = file_get_contents('https://framagit.org/qwertygc/Wiki/raw/master/version');
    if ($new > $old) {
        if (!is_dir('backups')) {
            mkdir('backups');
        }
        $filename = 'backups/backup-'.date('YmdHis', time()).'.zip';
        ZipDir($filename, 'data/');
        mylog('Backup: '.$filename);

        $fp = fopen('Wiki-'.$new.'.zip', 'w+');
        if ($fp === false) {
            throw new Exception('Could not open: ' . $saveTo);
        }
        $ch = curl_init('https://framagit.org/qwertygc/Wiki/-/archive/master/Wiki-master.zip');
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_exec($ch);
        if (curl_errno($ch)) {
            throw new Exception(curl_error($ch));
        }
        $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        fclose($fp);
        if ($statusCode == 200) {
            echo 'Downloaded!';
        } else {
            echo "Status Code: " . $statusCode;
        }

        $zipArchive = new ZipArchive();
        $result = $zipArchive->open('Wiki-'.$new.'.zip');
        if ($result === true) {
            $zipArchive->extractTo(getcwd());
            $zipArchive ->close();
            echo 'OK file is extracted';
        } else {
            echo 'ERROR for extract file';
        }
        recurseCopy('Wiki-master', '.');
        erased_dir('Wiki-master');
        mylog('UPDATE: '.$old.' to '.$new);

        redirect($config['root'].'/?do=config');
    } else {
        redirect($config['root'].'/?do=config');
    }
}

function url($id)
{
    global $config;
    if ($config['urlrewrite'] == true) {
        $url = $config['root'].'/'.$id;
    } else {
        $url = $config['root'].'/'.'?id='.$id;
    }
    return $url;
}
